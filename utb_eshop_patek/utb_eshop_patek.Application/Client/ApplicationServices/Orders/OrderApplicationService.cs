﻿using System;
using System.Collections.Generic;
using System.Text;

namespace utb_eshop_patek.Application.Client.ApplicationServices.Orders
{
    class OrderApplicationService : IOrderApplicationService
    {
        private readonly IOrderApplicationService _orderService;

        public OrderApplicationService(IOrderApplicationService orderService)
        {
            _orderService = orderService;
        }

        public void CreateOrder(int userID, string userTrackingCode)
        {
            _orderService.CreateOrder(userID, userTrackingCode);
        }
    }
}
