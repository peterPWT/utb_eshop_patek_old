﻿using AutoMapper;
using utb_eshop_patek.Application.Admin.ViewModels.Products;
using utb_eshop_patek.Domain.Entities.Products;

namespace utb_eshop_patek.Application.Configuration.Profiles.Products
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMaps();
        }

        private void CreateMaps()
        {
            CreateMap<Product, ProductViewModel>().ReverseMap();
        }
    }
}
