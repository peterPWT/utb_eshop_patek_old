﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Domain.Entities.Orders;

namespace utb_eshop_patek.Infrastructure.Data.Configurations.Orders
{
    public class OrderItemConfiguration : IEntityTypeConfiguration<OrderItem>
    {
        public void Configure(EntityTypeBuilder<OrderItem> builder)
        {
            builder.ToTable("OrderItems", "Web");
            builder.HasKey(e=>e.ID);
            builder.HasOne(e => e.Product)
                .WithMany()
                .IsRequired()
                .HasForeignKey(e => e.ProductID)
                .OnDelete(DeleteBehavior.Cascade);
                

        }
    }
}
